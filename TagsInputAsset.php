<?php

namespace studiosite\yii2tagsinput;

use Yii;
use yii\web\AssetBundle;

/**
 * Бандл стилелей-скптов
 *
 * @see https://github.com/bootstrap-tagsinput/bootstrap-tagsinput
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
class TagsInputAsset extends AssetBundle 
{
    /**
    * @var string Src path
    */
    public $sourcePath = '@bower/bootstrap-tagsinput';

    /**
    * @var string[] Include js files
    */
    public $js = [
        'dist/bootstrap-tagsinput.js'
    ];

    /**
    * @var string[] Include css(scss|less) files
    */
    public $css = [
        'dist/bootstrap-tagsinput.css'
    ];

    /**
    * @var string[] Asset bundles depends
    */
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset'
    ];
}
