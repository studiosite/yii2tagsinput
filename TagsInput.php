<?php

namespace studiosite\yii2tagsinput;

use yii\helpers\Html;
use yii\bootstrap\InputWidget;
use yii\helpers\Json;

/**
 * Поле ввода для тегов
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
class TagsInput extends InputWidget
{
    /**
    * @var array Опции плагина
    * @see http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/#options
    */
    public $clientOptions = [];

    /**
    * @var array События плагина
    * @see http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/#events
    */
    public $clientEvents = [];

    /**
    * Инициализация виджета, присвоение id, регистрация бандла
    */
    public function init()
    {
        parent::init();

        if (!isset($this->options['id'])) {
            if ($this->hasModel()) {
                $this->options['id'] = Html::getInputId($this->model, $this->attribute);
            } else {
                $this->options['id'] = $this->getId();
            }
        }

        TagsInputAsset::register($this->getView());
        
        $this->registerScript();
        $this->registerEvent();
    }

    /**
    * Отрисовка виджета
    */
    public function run()
    {
        if ($this->hasModel()) {
            echo Html::activeInput('text', $this->model, $this->attribute, $this->options);
        } else {
            echo Html::input('text', $this->name, $this->value, $this->options);
        }
    }

    /**
    * Регистрация плагина
    */
    public function registerScript()
    {
        $clientOptions = empty($this->clientOptions) ? '' : Json::encode($this->clientOptions);
        $js = "jQuery('#{$this->options["id"]}').tagsinput({$clientOptions});";
        $this->getView()->registerJs($js);
    }

    /**
    * Регистрация событий
    */
    public function registerEvent()
    {
        if (!empty($this->clientEvents)) {
            $js = [];
            foreach ($this->clientEvents as $event => $handle) {
                $js[] = "jQuery('#{$this->options["id"]}').on('$event',$handle);";
            }
            $this->getView()->registerJs(implode(PHP_EOL, $js));
        }
    }
}