Bootstrap Tags Input
=================

Ассетс бадл и виджет ввода [Bootstrap 3 Tags Input](https://github.com/bootstrap-tagsinput/bootstrap-tagsinput)
## Установка

Предпочтительный способ установить это расширение через композитор. [composer](http://getcomposer.org/download/).

```
"studiosite/yii2tagsinput": "*"
```

в секции ```require``` `composer.json` файла.

## Использование

В отображении 

```php
use studiosite\yii2tagsinput\TagsInput;

echo $form->field($model, 'tagNames')->widget(TagsInput::classname(), [
  'clientOptions' => [
  	'trimValue' => true,
  	'tagClass' => 'label label-primary'
  ]
]);
```